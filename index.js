const express = require("express");
const cors = require('cors');
const expressWs = require("express-ws");
const {nanoid} = require("nanoid");

const app = express();
expressWs(app);

app.use(express.json())
app.use(cors());

app.get("/", (req, res) => {
    res.send("Main page");
});

const activeConnections = {};
const pixels = [];

app.ws("/canvas", (ws, req) => {
    const id = nanoid();
    console.log("Client connected! id = " + id);
    activeConnections[id] = ws;

    ws.on("message", msg => {
        const dataPixel = JSON.parse(msg);
        switch(dataPixel.type) {
            case "GET_ALL_PIXELS":
                ws.send(JSON.stringify({type: "ALL_PIXELS", pixels}));
                break;
            case "CREATE_PICTURE":
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    pixels.push({
                        pixels: dataPixel.pixel
                    });
                    conn.send(JSON.stringify({
                        type: "NEW_PIXEL",
                        pixel: {
                            pixels: dataPixel.pixel
                        }
                    }));
                });
                break;
            default:
                console.log("Unknown message type:", dataPixel.type);
        }
    });

    ws.on("close", msg => {
        console.log("Client disconnected! id =" + id);
        delete activeConnections[id];
    });
});

app.listen(8000, () => {
    console.log("Server started at http://localhost:8000");
});